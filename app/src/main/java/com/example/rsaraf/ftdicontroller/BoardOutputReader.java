package com.example.rsaraf.ftdicontroller;


/**
 * Created by rsaraf on 4/5/16.
 */
public class BoardOutputReader {

    public static String readBytes(String text) {
        String[] rchar = text.split(" ");
        StringBuilder reader = new StringBuilder("");
        reader.append("Conversion of " + text + " with length " + rchar.length + ":");
        try {
            if (rchar != null) {
                if (rchar.length > 0) {
                    if (rchar[0] == "p") {
                        reader.append(" write");
                    } else if (rchar[0] == "o") {
                        reader.append(" status");
                    } else if (rchar[0] == "q") {
                        reader.append(" sensor state");
                    }
                }

                reader.append(" lock " + rchar[1]);

                if (rchar[2] == "w") {
                    reader.append(" successful");
                } else if (rchar[2] == "1") {
                    reader.append(" is open");
                } else if (rchar[2] == "0") {
                    reader.append(" is close");
                }

                if (rchar[0] == "q") {
                    if (rchar[2] == "N" && rchar[3] == "O") {
                        reader.append(" Sensor1 no object detected");
                    } else if (rchar[2] == "O" && rchar[3] == "D") {
                        reader.append(" Sensor1 object detected");
                    }

                    if (rchar[4] == "N" && rchar[5] == "O") {
                        reader.append(" Sensor2 no object detected");
                    } else if (rchar[4] == "O" && rchar[5] == "D") {
                        reader.append(" Sensor2 object detected");
                    }
                }
            }
        } catch (Exception ex) {
            reader.append("Exception in output reading:" + ex.getMessage());
        }
        return reader.toString();

    }
}
